package com.example.vetistech.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.vetistech.frameworks.room.VetistechDB
import com.example.vetistech.repository.ReadingsRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Singleton
class ReadingsViewModel constructor(application: Application) : AndroidViewModel(application) {

    private val vetistechDB = VetistechDB.getInstance(context = application.applicationContext)
    private val readingsRepository = ReadingsRepository(db = vetistechDB)

    val sensorReadings = readingsRepository.sensorReadings
    val hasAPIError = readingsRepository.hasAPIError

    fun getSensorReadings(platform_id: Int, default_row_id: Int = 0) {
        viewModelScope.launch {
            readingsRepository.getSensorReadings(platform_id = platform_id, default_row_id = default_row_id)
        }
    }

    fun deleteSensorReadings() {
        viewModelScope.launch {
            readingsRepository.deleteSensorReadings()
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ReadingsViewModel::class.java)) {
                return ReadingsViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct ViewModel ReadingsViewModel::class.java")
        }
    }

}