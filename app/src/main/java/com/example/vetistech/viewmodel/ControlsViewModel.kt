package com.example.vetistech.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.vetistech.repository.ControlsRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Singleton
class ControlsViewModel constructor(application: Application) : AndroidViewModel(application) {

    private val controlsRepository = ControlsRepository()

    val hasAPIError = controlsRepository.hasAPIError
    val controlsData = controlsRepository.controlsData

    fun getControls(platform_id: Int) {
        viewModelScope.launch {
            controlsRepository.getControls(platform_id = platform_id)
        }
    }

    fun postControls(platform_id: Int, value: String) {
        viewModelScope.launch {
            controlsRepository.postControls(platform_id = platform_id, value = value)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ControlsViewModel::class.java)) {
                return ControlsViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct ViewModel ControlsViewModel::class.java")
        }
    }

}