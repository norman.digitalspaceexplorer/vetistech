package com.example.vetistech.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.vetistech.frameworks.room.VetistechDB
import com.example.vetistech.repository.FCMRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Singleton
class FCMViewModel constructor(application: Application) : AndroidViewModel(application) {

    private val vetistechDB = VetistechDB.getInstance(context = application.applicationContext)
    private val fcmRepository = FCMRepository(db = vetistechDB)

    val fcmData = fcmRepository.fcmData
    val hasAPIError = fcmRepository.hasAPIError

    fun putFCMToken(token: String, token_id: Int = -1) {
        viewModelScope.launch {
            fcmRepository.putFCMToken(token = token, token_id = token_id)
        }
    }

    fun deleteFCMToken() {
        viewModelScope.launch {
            fcmRepository.deleteFCMToken()
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(FCMViewModel::class.java)) {
                return FCMViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct ViewModel FCMViewModel::class.java")
        }
    }

}