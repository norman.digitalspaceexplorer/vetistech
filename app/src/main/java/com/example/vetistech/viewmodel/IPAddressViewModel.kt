package com.example.vetistech.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.vetistech.frameworks.model.IPAddressModel
import com.example.vetistech.frameworks.room.VetistechDB
import com.example.vetistech.repository.IPAddressRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class IPAddressViewModel constructor(application: Application) : AndroidViewModel(application) {

    private val db = VetistechDB.getInstance(context = application.applicationContext)
    private val ipAddressRepository = IPAddressRepository(db = db)

    val ipAddressModel = ipAddressRepository.ipAddressModel

    fun insertIPAddress(ipAddressModel: IPAddressModel) {
        viewModelScope.launch {
            ipAddressRepository.insertIPAddress(ipAddressModel = ipAddressModel)
        }
    }

    fun updateIPAddress(ipAddressModel: IPAddressModel) {
        viewModelScope.launch {
            ipAddressRepository.updateIPAddress(ipAddressModel = ipAddressModel)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(IPAddressViewModel::class.java)) {
                return IPAddressViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct ViewModel IPAddressViewModel::class.java")
        }
    }

}