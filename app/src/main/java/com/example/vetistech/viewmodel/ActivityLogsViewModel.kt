package com.example.vetistech.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.vetistech.frameworks.room.VetistechDB
import com.example.vetistech.repository.ActivityLogsRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Singleton
class ActivityLogsViewModel constructor(application: Application) : AndroidViewModel(application) {

    private val vetistechDB = VetistechDB.getInstance(context = application.applicationContext)
    private val logsRepository = ActivityLogsRepository(db = vetistechDB)

    val hasAPIError = logsRepository.hasAPIError
    val activityLogsData = logsRepository.activityLogs

    fun getActivityLogs(platform_id: Int = 1, page: Int = 0) {
        viewModelScope.launch {
            logsRepository.getActivityLogs(page = page, platform_id = platform_id)
        }
    }

    fun deleteActivityLogs() {
        viewModelScope.launch {
            logsRepository.deleteActivityLogs()
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ActivityLogsViewModel::class.java)) {
                return ActivityLogsViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct ViewModel ActivityLogsViewModel::class.java")
        }
    }

}