package com.example.vetistech

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.vetistech.databinding.RecyclerviewLogsBinding
import com.example.vetistech.frameworks.model.LogsData

class LogsAdapter : RecyclerView.Adapter<LogsAdapter.ViewHolder>() {

    var activityLogs: List<LogsData> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class ViewHolder(val binding: RecyclerviewLogsBinding) : RecyclerView.ViewHolder(binding.root) {
        companion object {
            @LayoutRes
            val LAYOUT = R.layout.recyclerview_logs
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val withBinding: RecyclerviewLogsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), ViewHolder.LAYOUT, parent, false)
        return ViewHolder(binding = withBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.also {
            it.activityLogsData = activityLogs[position]
        }
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return activityLogs.size
    }

}