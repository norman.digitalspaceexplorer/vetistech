package com.example.vetistech

import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.vetistech.app.BaseActivity
import com.example.vetistech.cloud.CloudMessaging
import com.example.vetistech.databinding.ActivityLogsBinding
import com.example.vetistech.viewmodel.ActivityLogsViewModel

class ActivityLogs : BaseActivity(classInherited = "ActivityLogs"),
    NestedScrollView.OnScrollChangeListener,
    SwipeRefreshLayout.OnRefreshListener {

    private lateinit var binding: ActivityLogsBinding
    private lateinit var viewModel: ActivityLogsViewModel
    private lateinit var logsAdapter: LogsAdapter

    private var currentPage: Int = 1

    companion object {
        const val defaultCurrentPage: Int = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.title_activity_logs)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_logs);

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        logsAdapter = LogsAdapter()
        binding.recyclerViewActivityLogs.adapter = logsAdapter
        viewModel = ViewModelProvider(this, ActivityLogsViewModel.Factory(application))
            .get(ActivityLogsViewModel::class.java)

        setObservableModels()

        if (isNetworkAvailable()) {
            currentPage = defaultCurrentPage
            viewModel.deleteActivityLogs()
            viewModel.getActivityLogs(platform_id = 1, page = currentPage)
        }
    }

    private fun setObservableModels() {
        viewModel.hasAPIError.observe(this, {
            it?.let {
                if (it.hasAPIError) {
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.activityLogsData.observe(this, {
            it?.let {
                if (it.isNotEmpty()) {
                    binding.scrollView.setOnScrollChangeListener(this)
                    binding.swipeRefreshLayout.setOnRefreshListener(this)
                    binding.swipeRefreshLayout.isRefreshing = false
                    binding.progressCircular.visibility = View.INVISIBLE
                    binding.progressHorizontal.visibility = View.VISIBLE
                    logsAdapter.activityLogs = it
                    currentPage = currentPage.plus(1)
                    title = "${getString(R.string.title_activity_logs)} (${it.size})"
                }
                return@observe
            }
            binding.swipeRefreshLayout.setOnRefreshListener(null)
            binding.progressCircular.visibility = View.VISIBLE
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(CloudMessaging.INTENT_ACTION_SENT_MESSAGE)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
        if (scrollY == (v!!.getChildAt(0).measuredHeight-v.measuredHeight)) {
            if (isNetworkAvailable()) {
                binding.progressHorizontal.visibility = View.VISIBLE
                viewModel.getActivityLogs(platform_id = 1, page = currentPage)
                return
            }
            binding.progressHorizontal.visibility = View.GONE
        }
    }

    override fun onRefresh() {
        if (isNetworkAvailable()) {
            currentPage = defaultCurrentPage
            binding.swipeRefreshLayout.isRefreshing = true
            viewModel.deleteActivityLogs()
            viewModel.getActivityLogs(platform_id = 1, page = currentPage)
            return
        }
        binding.swipeRefreshLayout.isRefreshing = false
    }

}