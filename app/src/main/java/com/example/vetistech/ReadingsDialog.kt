package com.example.vetistech

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.vetistech.app.BaseFragment
import com.example.vetistech.databinding.BottomsheetDialogReadingsBinding
import com.example.vetistech.frameworks.model.ReadingsData
import com.example.vetistech.viewmodel.ReadingsViewModel

class ReadingsDialog : BaseFragment() {

    private lateinit var binding: BottomsheetDialogReadingsBinding
    private lateinit var viewModel: ReadingsViewModel

    private var alreadyHasData: Boolean = false
    private var defaultRowID: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottomsheet_dialog_readings, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.sensorReadingsData = ReadingsData()
        if (isNetworkAvailable()) {
            viewModel = ViewModelProvider(this, ReadingsViewModel.Factory(application = activity!!.application))
                .get(ReadingsViewModel::class.java)
            viewModel.deleteSensorReadings()
            viewModel.getSensorReadings(platform_id = 1)
            setObservableModels()
            return
        }
        Toast.makeText(context, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
    }

    private fun setObservableModels() {
        viewModel.sensorReadings.observe(viewLifecycleOwner, {
            it?.let {
                if (it.isNotEmpty()) {
                    binding.relativeLayoutContainer.visibility = View.VISIBLE
                    binding.progressCircular.visibility = View.GONE
                    binding.sensorReadingsData = it[0]
                    alreadyHasData = true
                    defaultRowID = it[0].id!!
                    Handler(Looper.getMainLooper()).postDelayed({
                        viewModel.getSensorReadings(platform_id = 1, default_row_id = defaultRowID)
                    }, 1000)
                }
            }
        })
        viewModel.hasAPIError.observe(viewLifecycleOwner, {
            it?.let {
                if (it.hasAPIError) {
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

}