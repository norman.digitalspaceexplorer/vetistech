package com.example.vetistech.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.vetistech.frameworks.entity.asIPAddressEntity
import com.example.vetistech.frameworks.entity.asIPAddressModel
import com.example.vetistech.frameworks.entity.asUpdateIPAddressEntity
import com.example.vetistech.frameworks.model.IPAddressModel
import com.example.vetistech.frameworks.room.VetistechDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class IPAddressRepository constructor(private val db: VetistechDB) {

    val ipAddressModel: LiveData<List<IPAddressModel>> = Transformations.map(db.ipAddressDao.getIPAddress())
        { it?.asIPAddressModel() }

    suspend fun insertIPAddress(ipAddressModel: IPAddressModel) {
        withContext(Dispatchers.IO) {
            db.ipAddressDao.insert(ipAddressEntity = ipAddressModel.asIPAddressEntity())
        }
    }

    suspend fun updateIPAddress(ipAddressModel: IPAddressModel) {
        withContext(Dispatchers.IO) {
            db.ipAddressDao.update(ipAddressEntity = ipAddressModel.asUpdateIPAddressEntity())
        }
    }

}