package com.example.vetistech.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.vetistech.frameworks.entity.asFCMData
import com.example.vetistech.frameworks.entity.asFCMEntity
import com.example.vetistech.frameworks.model.FCMData
import com.example.vetistech.frameworks.network.APIErrorHandler
import com.example.vetistech.frameworks.network.NetworkFCM
import com.example.vetistech.frameworks.room.VetistechDB
import com.example.vetistech.wrappers.APIErrorWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class FCMRepository constructor(val db: VetistechDB) {

    private val _hasAPIError: MutableLiveData<APIErrorWrapper> = MutableLiveData()
    val hasAPIError: LiveData<APIErrorWrapper> get() = _hasAPIError

    val fcmData: LiveData<FCMData> = Transformations.map(db.fcmDao.getFCMTokens())
        { it?.asFCMData() }

    suspend fun putFCMToken(token: String, token_id: Int) {
        withContext(Dispatchers.IO) {
            try {
                val response = NetworkFCM.service.putFCMTokenAsync(
                    platform_id = 1, token_id = token_id, token = token
                ).await()
                Timber.e("Response: $response")
                db.fcmDao.insert(fcmEntity = response.asFCMEntity())
            } catch (e: Throwable) {
                val errorMessage = APIErrorHandler(error = e).checkError().getErrorMessageResponse()
                _hasAPIError.postValue(APIErrorWrapper(hasAPIError = true, errorMessage = errorMessage))
            }
        }
    }

    suspend fun deleteFCMToken() {
        withContext(Dispatchers.IO) {
            db.fcmDao.deleteAllFCM()
        }
    }

}