package com.example.vetistech.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.vetistech.frameworks.model.ControlsModel
import com.example.vetistech.frameworks.network.APIErrorHandler
import com.example.vetistech.frameworks.network.NetworkControls
import com.example.vetistech.wrappers.APIErrorWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class ControlsRepository {

    private val _hasAPIError: MutableLiveData<APIErrorWrapper> = MutableLiveData()
    val hasAPIError: LiveData<APIErrorWrapper> get() = _hasAPIError

    private val _controlsData: MutableLiveData<ControlsModel> = MutableLiveData()
    val controlsData: LiveData<ControlsModel> get() = _controlsData

    suspend fun getControls(platform_id: Int) {
        withContext(Dispatchers.IO) {
            try {
                val response = NetworkControls.service.getControlsAsync(
                    platform_id = platform_id
                ).await()
                Timber.e("Controls Response: $response")
                _controlsData.postValue(response)
            } catch (e: Throwable) {
                val errorMessage = APIErrorHandler(error = e).checkError().getErrorMessageResponse()
                _hasAPIError.postValue(APIErrorWrapper(hasAPIError = true, errorMessage = errorMessage))
            }
        }
    }

    suspend fun postControls(platform_id: Int, value: String) {
        withContext(Dispatchers.IO) {
            try {
                val response = NetworkControls.service.postControlsAsync(
                    platform_id = platform_id, value = value
                ).await()
                Timber.e("Controls Response: $response")
                _controlsData.postValue(response)
            } catch (e: Throwable) {
                val errorMessage = APIErrorHandler(error = e).checkError().getErrorMessageResponse()
                _hasAPIError.postValue(APIErrorWrapper(hasAPIError = true, errorMessage = errorMessage))
            }
        }
    }

}