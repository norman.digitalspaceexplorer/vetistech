package com.example.vetistech.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.vetistech.frameworks.entity.asReadingsData
import com.example.vetistech.frameworks.entity.asReadingsEntity
import com.example.vetistech.frameworks.model.ReadingsData
import com.example.vetistech.frameworks.network.APIErrorHandler
import com.example.vetistech.frameworks.network.NetworkSensors
import com.example.vetistech.frameworks.room.VetistechDB
import com.example.vetistech.wrappers.APIErrorWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class ReadingsRepository constructor(private val db: VetistechDB) {

    private val _hasAPIError: MutableLiveData<APIErrorWrapper> = MutableLiveData()
    val hasAPIError: LiveData<APIErrorWrapper> get() = _hasAPIError

    val sensorReadings: LiveData<List<ReadingsData>> = Transformations.map(db.sensorReadingsDao.getAllReadings()) {
        it?.asReadingsData()
    }

    suspend fun getSensorReadings(platform_id: Int, default_row_id: Int) {
        withContext(Dispatchers.IO) {
            try {
                val response = NetworkSensors.service.getSensorReadingsAsync(
                    platform_id = platform_id
                ).await()
                Timber.e("Sensor Response: $response")
                db.sensorReadingsDao.insert(readingsEntity = response.data.asReadingsEntity(default_row_id))
            } catch (e: Throwable) {
                val errorMessage = APIErrorHandler(error = e).checkError().getErrorMessageResponse()
                _hasAPIError.postValue(APIErrorWrapper(hasAPIError = true, errorMessage = errorMessage))
            }
        }
    }

    suspend fun deleteSensorReadings() {
        withContext(Dispatchers.IO) {
            db.sensorReadingsDao.deleteAllReadings()
        }
    }

}