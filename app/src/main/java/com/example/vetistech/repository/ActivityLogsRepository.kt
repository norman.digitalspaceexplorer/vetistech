package com.example.vetistech.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.vetistech.frameworks.entity.asActivityLogsEntity
import com.example.vetistech.frameworks.entity.asLogsData
import com.example.vetistech.frameworks.model.LogsData
import com.example.vetistech.frameworks.network.APIErrorHandler
import com.example.vetistech.frameworks.network.NetworkLogs
import com.example.vetistech.frameworks.room.VetistechDB
import com.example.vetistech.wrappers.APIErrorWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class ActivityLogsRepository constructor(val db: VetistechDB) {

    private val _hasAPIError: MutableLiveData<APIErrorWrapper> = MutableLiveData()
    val hasAPIError: LiveData<APIErrorWrapper> get() = _hasAPIError

    val activityLogs: LiveData<List<LogsData>> = Transformations.map(db.logsDao.getAllLogs()) {
        it?.asLogsData()
    }

    suspend fun getActivityLogs(page: Int, platform_id: Int) {
        withContext(Dispatchers.IO) {
            try {
                val response = NetworkLogs.service.getActivityLogsAsync(
                    platform_id = platform_id, page = page
                ).await()
                Timber.e("Response: $response")
                db.logsDao.insert(activityLogsEntity = response.data.asActivityLogsEntity())
            } catch (e: Throwable) {
                val errorMessage = APIErrorHandler(error = e).checkError().getErrorMessageResponse()
                _hasAPIError.postValue(APIErrorWrapper(hasAPIError = true, errorMessage = errorMessage))
            }
        }
    }

    suspend fun deleteActivityLogs() {
        withContext(Dispatchers.IO) {
            db.logsDao.deleteAllLogs()
        }
    }

}