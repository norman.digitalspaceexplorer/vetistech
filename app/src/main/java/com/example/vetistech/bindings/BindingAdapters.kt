package com.example.vetistech.bindings

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.vetistech.frameworks.model.LogsData
import com.example.vetistech.frameworks.model.ReadingsData
import com.example.vetistech.helpers.HelperMainListener
import com.google.android.material.card.MaterialCardView

@BindingAdapter("performMainCardViewClick")
fun performMainCardViewClick(cardView: MaterialCardView, helper: HelperMainListener) =
    cardView.setOnClickListener(helper)

@BindingAdapter("setLogsDateTime")
fun setLogsDateTime(textView: TextView, logsData: LogsData) {
    val date = "DATE: ${logsData.log_date} ${logsData.log_time}"
    textView.text = date
}

@BindingAdapter("setHumidityPercentage")
fun setHumidityPercentage(textView: TextView, logsData: LogsData) {
    val humidityPercentage = "${logsData.humidity} %"
    textView.text = humidityPercentage
}

@BindingAdapter("setHumidityReadingsPercentage")
fun setHumidityReadingsPercentage(textView: TextView, readingsData: ReadingsData) {
    val humidityPercentage = "${readingsData.humidity} %"
    textView.text = humidityPercentage
}

@BindingAdapter("setFanValue")
fun setFanValue(textView: TextView, logsData: LogsData) {
    val exhaustFanValue = "FAN: ${logsData.exhaust_fan}"
    textView.text = exhaustFanValue
}

@BindingAdapter("setPumpValue")
fun setPumpValue(textView: TextView, logsData: LogsData) {
    val mistPumpValue = "PUMP: ${logsData.mist_pump}"
    textView.text = mistPumpValue
}

@BindingAdapter("setHeaterValue")
fun setHeaterValue(textView: TextView, logsData: LogsData) {
    val heaterValue = "HEATER: ${logsData.heater}"
    textView.text = heaterValue
}