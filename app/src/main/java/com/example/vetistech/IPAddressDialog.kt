package com.example.vetistech

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.vetistech.app.BaseFragment
import com.example.vetistech.databinding.BottomsheetDialogIpBinding
import com.example.vetistech.frameworks.model.IPAddressModel
import com.example.vetistech.viewmodel.IPAddressViewModel
import timber.log.Timber

class IPAddressDialog : BaseFragment() {

    lateinit var binding: BottomsheetDialogIpBinding
    lateinit var viewModel: IPAddressViewModel

    private var hasAlreadyAnIPAddress: Boolean = false
    private var ipAddressID: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottomsheet_dialog_ip, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.e("OnViewCreated ...")
        viewModel = ViewModelProvider(this, IPAddressViewModel.Factory(application = requireActivity().application))
            .get(IPAddressViewModel::class.java)
        binding.buttonAddIPAddress.setOnClickListener {
            val ipAddress: String = binding.editTextIpAddress.text.toString()
            if (ipAddress.isNotEmpty()) {
                if (isNetworkAvailable()) {
                    setIPAddress(hasAlreadyAnIPAddress)
                    dismiss()
                    Handler(Looper.getMainLooper()).postDelayed({
                        it.context.startActivity(
                            Intent(it.context, CCTVActivity::class.java)
                                .putExtra("IP_ADDRESS", binding.editTextIpAddress.text.toString())
                        )
                    }, 500)
                    return@setOnClickListener
                }
                Toast.makeText(context, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            Toast.makeText(context, getString(R.string.please_enter_ip_address), Toast.LENGTH_SHORT).show()
        }
        binding.buttonOpenBrowser.setOnClickListener {
            val ipAddress: String = binding.editTextIpAddress.text.toString()
            if (ipAddress.isNotEmpty()) {
                if (isNetworkAvailable()) {
                    setIPAddress(hasAlreadyAnIPAddress)
                    dismiss()
                    Handler(Looper.getMainLooper()).postDelayed({
                        val url = "http://${binding.editTextIpAddress.text}"
                        it.context.startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)))
                    }, 500)
                    return@setOnClickListener
                }
                Toast.makeText(context, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            Toast.makeText(context, getString(R.string.please_enter_ip_address), Toast.LENGTH_SHORT).show()
        }
        setObservableModels()
    }

    private fun setObservableModels() {
        viewModel.ipAddressModel.observe(viewLifecycleOwner, {
            it?.let {
                if (it.isNotEmpty()) {
                    hasAlreadyAnIPAddress = true
                    ipAddressID = it[0].id
                    binding.editTextIpAddress.setText(it[0].ip_address)
                }
            }
        })
    }

    private fun setIPAddress(hasAlreadyAnIPAddress: Boolean) {
        val ipAddress = binding.editTextIpAddress.text.toString()
        if (hasAlreadyAnIPAddress) {
            viewModel.updateIPAddress(IPAddressModel(id = ipAddressID, ip_address = ipAddress))
        } else {
            viewModel.insertIPAddress(IPAddressModel(id = 0, ip_address = ipAddress))
        }
    }

}