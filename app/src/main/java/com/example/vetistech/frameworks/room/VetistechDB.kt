package com.example.vetistech.frameworks.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.vetistech.frameworks.dao.ActivityLogsDao
import com.example.vetistech.frameworks.dao.FCMDao
import com.example.vetistech.frameworks.dao.IPAddressDao
import com.example.vetistech.frameworks.dao.SensorReadingsDao
import com.example.vetistech.frameworks.entity.ActivityLogsEntity
import com.example.vetistech.frameworks.entity.FCMEntity
import com.example.vetistech.frameworks.entity.IPAddressEntity
import com.example.vetistech.frameworks.entity.ReadingsEntity

@Database(entities = [
    FCMEntity::class,
    ActivityLogsEntity::class,
    ReadingsEntity::class,
    IPAddressEntity::class
 ], version = 4, exportSchema = false)
abstract class VetistechDB : RoomDatabase() {

    abstract val fcmDao: FCMDao
    abstract val logsDao: ActivityLogsDao
    abstract val sensorReadingsDao: SensorReadingsDao
    abstract val ipAddressDao: IPAddressDao

    companion object {
        @Volatile
        private var INSTANCE: VetistechDB? = null
        fun getInstance(context: Context) : VetistechDB {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        VetistechDB::class.java, "Vetistech")
                        .fallbackToDestructiveMigration()
                        .build()
                }
                INSTANCE = instance
                return instance
            }
        }
    }

}