package com.example.vetistech.frameworks.network

import com.example.vetistech.frameworks.model.ActivityLogsModel
import com.example.vetistech.interceptors.BaseInterceptor
import com.example.vetistech.interceptors.NetworkInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ActivityLogsAPI {
    @GET("api/activity/log")
    fun getActivityLogsAsync(
        @Query("platform_id") platform_id: Int,
        @Query("page") page: Int
    ) : Deferred<ActivityLogsModel>
}

object NetworkLogs {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BaseInterceptor.BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshiBuilder))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(NetworkInterceptor(OkHttpClient.Builder()).getInterceptor())
        .build()
    val service: ActivityLogsAPI = retrofit.create(ActivityLogsAPI::class.java)
}