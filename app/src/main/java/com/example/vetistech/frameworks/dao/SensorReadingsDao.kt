package com.example.vetistech.frameworks.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.vetistech.frameworks.entity.ReadingsEntity

@Dao
interface SensorReadingsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(readingsEntity: List<ReadingsEntity>)

    @Query("DELETE FROM READINGS_ENTITY")
    fun deleteAllReadings()

    @Query("SELECT * FROM READINGS_ENTITY")
    fun getAllReadings() : LiveData<List<ReadingsEntity>>

}