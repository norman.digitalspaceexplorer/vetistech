package com.example.vetistech.frameworks.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.vetistech.frameworks.entity.IPAddressEntity

@Dao
interface IPAddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ipAddressEntity: IPAddressEntity)

    @Update
    fun update(ipAddressEntity: IPAddressEntity)

    @Query("SELECT * FROM IP_ADDRESS_ENTITY ORDER BY id LIMIT 1")
    fun getIPAddress() : LiveData<List<IPAddressEntity>>

}