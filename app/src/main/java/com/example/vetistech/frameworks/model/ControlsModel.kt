package com.example.vetistech.frameworks.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ControlsModel constructor(
    val status: Int,
    val data: ControlsData
)

@JsonClass(generateAdapter = true)
data class ControlsData constructor(
    val mist_pump: String,
    val user_agent: String
)