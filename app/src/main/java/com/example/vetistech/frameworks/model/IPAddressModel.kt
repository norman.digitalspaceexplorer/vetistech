package com.example.vetistech.frameworks.model

data class IPAddressModel constructor(
    val id: Int,
    val ip_address: String
)