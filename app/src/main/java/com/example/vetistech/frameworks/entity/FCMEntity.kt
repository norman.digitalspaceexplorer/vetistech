package com.example.vetistech.frameworks.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.vetistech.frameworks.model.FCMData
import com.example.vetistech.frameworks.model.FCMModel

@Entity(tableName = "FCM_ENTITY")
data class FCMEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val token_id: Int,
    val token: String,
    val user_agent: String
)

fun FCMModel.asFCMEntity() : FCMEntity {
    return FCMEntity(
        id = 0, token_id = this.data.id,
        token = this.data.token, user_agent = this.data.user_agent
    )
}

fun FCMEntity.asFCMData() : FCMData {
    return FCMData(
        id = this.token_id, token = this.token,
        user_agent = this.user_agent
    )
}