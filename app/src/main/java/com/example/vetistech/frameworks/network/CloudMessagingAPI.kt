package com.example.vetistech.frameworks.network

import com.example.vetistech.frameworks.model.FCMModel
import com.example.vetistech.interceptors.BaseInterceptor
import com.example.vetistech.interceptors.NetworkInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.PUT
import retrofit2.http.Query

interface CloudMessagingAPI {
    @FormUrlEncoded
    @PUT("api/notifications/custom")
    fun putFCMTokenAsync(
        @Query("platform_id") platform_id: Int,
        @Field("token") token: String,
        @Field("token_id") token_id: Int
    ) : Deferred<FCMModel>
}

val moshiBuilder: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

object NetworkFCM {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BaseInterceptor.BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshiBuilder))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(NetworkInterceptor(OkHttpClient.Builder()).getInterceptor())
        .build()
    val service: CloudMessagingAPI = retrofit.create(CloudMessagingAPI::class.java)
}