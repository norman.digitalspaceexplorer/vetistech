package com.example.vetistech.frameworks.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.vetistech.frameworks.model.ReadingsData

@Entity(tableName = "READINGS_ENTITY")
data class ReadingsEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val row_id: Int,
    val temperature: String,
    val humidity: String,
    val moisture: String,
    val heat_index: String,
    val exhaust_fan: String,
    val mist_pump: String,
    val heater: String,
    val log_date: String,
    val log_time: String,
    val user_agent: String
)

fun List<ReadingsData>.asReadingsEntity(default_row_id: Int) : List<ReadingsEntity> {
    return map {
        ReadingsEntity(
            id = default_row_id,
            row_id = it.id!!,
            temperature = it.temperature!!,
            humidity = it.humidity!!,
            moisture = it.moisture!!,
            heat_index = it.heat_index!!,
            exhaust_fan = it.exhaust_fan!!,
            mist_pump = it.mist_pump!!,
            heater = it.heater!!,
            log_date = it.log_date!!,
            log_time = it.log_time!!,
            user_agent = it.user_agent!!
        )
    }
}

fun List<ReadingsEntity>.asReadingsData() : List<ReadingsData> {
    return map {
        ReadingsData(
            id = it.id, // Changed to ID instead or Row ID since we're not using row_id
            temperature = it.temperature,
            humidity = it.humidity,
            moisture = it.moisture,
            heat_index = it.heat_index,
            exhaust_fan = it.exhaust_fan,
            mist_pump = it.mist_pump,
            heater = it.heater,
            log_date = it.log_date,
            log_time = it.log_time,
            user_agent = it.user_agent
        )
    }
}