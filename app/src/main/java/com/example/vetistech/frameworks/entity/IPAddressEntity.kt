package com.example.vetistech.frameworks.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.vetistech.frameworks.model.IPAddressModel

@Entity(tableName = "IP_ADDRESS_ENTITY")
data class IPAddressEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val ip_address: String
)

fun IPAddressModel.asIPAddressEntity() : IPAddressEntity {
    return IPAddressEntity(
        id = 0,
        ip_address = this.ip_address
    )
}

fun IPAddressModel.asUpdateIPAddressEntity() : IPAddressEntity {
    return IPAddressEntity(
        id = this.id,
        ip_address = this.ip_address
    )
}

fun List<IPAddressEntity>.asIPAddressModel() : List<IPAddressModel> {
    return map {
        IPAddressModel(
            id = it.id,
            ip_address = it.ip_address
        )
    }
}