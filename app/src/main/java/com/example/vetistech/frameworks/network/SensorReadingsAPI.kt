package com.example.vetistech.frameworks.network

import com.example.vetistech.frameworks.model.ReadingsModel
import com.example.vetistech.interceptors.BaseInterceptor
import com.example.vetistech.interceptors.NetworkInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface SensorReadingsAPI {
    @GET("api/sensors")
    fun getSensorReadingsAsync(
        @Query("platform_id") platform_id: Int
    ) : Deferred<ReadingsModel>
}

object NetworkSensors {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BaseInterceptor.BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshiBuilder))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(NetworkInterceptor(OkHttpClient.Builder()).getInterceptor())
        .build()
    val service: SensorReadingsAPI = retrofit.create(SensorReadingsAPI::class.java)
}