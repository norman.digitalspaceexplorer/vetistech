package com.example.vetistech.frameworks.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ActivityLogsModel constructor(
    val status: Int,
    val message: String,
    val data: List<LogsData>
)

@JsonClass(generateAdapter = true)
data class LogsData constructor(
    val id: Int,
    val temperature: String,
    val humidity: String,
    val moisture: String,
    val heat_index: String,
    val exhaust_fan: String,
    val mist_pump: String,
    val heater: String,
    val log_date: String,
    val log_time: String,
    val user_agent: String
)