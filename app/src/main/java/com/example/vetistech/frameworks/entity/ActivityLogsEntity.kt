package com.example.vetistech.frameworks.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.vetistech.frameworks.model.LogsData

@Entity(tableName = "ACTIVITY_LOGS_ENTITY")
data class ActivityLogsEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val row_id: Int,
    val temperature: String,
    val humidity: String,
    val moisture: String,
    val heat_index: String,
    val exhaust_fan: String,
    val mist_pump: String,
    val heater: String,
    val log_date: String,
    val log_time: String,
    val user_agent: String
)

fun List<LogsData>.asActivityLogsEntity() : List<ActivityLogsEntity> {
    return map { logsData ->
        ActivityLogsEntity(
            id = 0,
            row_id = logsData.id,
            temperature = logsData.temperature,
            humidity = logsData.humidity,
            moisture = logsData.moisture,
            heat_index = logsData.heat_index,
            exhaust_fan = logsData.exhaust_fan,
            mist_pump = logsData.mist_pump,
            heater = logsData.heater,
            log_date = logsData.log_date,
            log_time = logsData.log_time,
            user_agent = logsData.user_agent
        )
    }
}

fun List<ActivityLogsEntity>.asLogsData() : List<LogsData> {
    return map { activityLogsEntity ->
        LogsData(
            id = activityLogsEntity.row_id,
            temperature = activityLogsEntity.temperature,
            humidity = activityLogsEntity.humidity,
            moisture = activityLogsEntity.moisture,
            heat_index = activityLogsEntity.heat_index,
            exhaust_fan = activityLogsEntity.exhaust_fan,
            mist_pump = activityLogsEntity.mist_pump,
            heater = activityLogsEntity.heater,
            log_date = activityLogsEntity.log_date,
            log_time = activityLogsEntity.log_time,
            user_agent = activityLogsEntity.user_agent
        )
    }
}