package com.example.vetistech.frameworks.network

import com.example.vetistech.frameworks.model.APIErrorModel
import com.google.gson.Gson
import retrofit2.HttpException
import java.lang.Exception
import java.net.ConnectException
import java.net.SocketException

class APIErrorHandler constructor(private val error: Throwable) {

    private var errorMessage: String = ""

    companion object {
        const val CONNECTION_ERROR_MESSAGE = "Server Connection Failed, Something went really bad."
        const val DEFAULT_ERROR_MESSAGE = "Something went wrong on our end, Please reach out to us as possible."
        const val SOCKET_ERROR_MESSAGE = "Request Time Out, Please Try Again."
        const val PARSING_ERROR_MESSAGE = "Cannot process your request this time, Something went wrong."
    }

    fun checkError() : APIErrorHandler {
        when (error) {
            is SocketException -> errorMessage = SOCKET_ERROR_MESSAGE
            is ConnectException -> errorMessage = CONNECTION_ERROR_MESSAGE
            is HttpException -> {
                errorMessage = try {
                    val errorResponse = error.response()?.errorBody()?.string()
                    when (error.code()) {
                        500 -> {
                            val errorResponseJson = Gson().fromJson(errorResponse, APIErrorModel::class.java)
                            "${errorResponseJson.message}, Code: ${errorResponseJson.status}"
                        } else -> {
                        DEFAULT_ERROR_MESSAGE
                    }
                    }
                } catch (e: Exception) {
                    PARSING_ERROR_MESSAGE
                }
            } else -> errorMessage = DEFAULT_ERROR_MESSAGE
        }
        return this;
    }

    fun getErrorMessageResponse() = errorMessage

}