package com.example.vetistech.frameworks.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.vetistech.frameworks.entity.FCMEntity

@Dao
interface FCMDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(fcmEntity: FCMEntity)

    @Update
    fun update(fcmEntity: FCMEntity)

    @Query("DELETE FROM FCM_ENTITY")
    fun deleteAllFCM()

    @Query("SELECT * FROM FCM_ENTITY")
    fun getFCMTokens() : LiveData<FCMEntity>

}