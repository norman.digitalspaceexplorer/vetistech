package com.example.vetistech.frameworks.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FCMModel constructor(
    val status: Int,
    val message: String,
    val data: FCMData
)

@JsonClass(generateAdapter = true)
data class FCMData constructor(
    val id: Int,
    @Json(name = "tokens")
    val token: String,
    val user_agent: String
)