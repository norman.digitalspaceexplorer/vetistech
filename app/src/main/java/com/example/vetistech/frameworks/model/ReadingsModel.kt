package com.example.vetistech.frameworks.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ReadingsModel constructor(
    val status: Int,
    val message: String,
    val data: List<ReadingsData>
)

@JsonClass(generateAdapter = true)
data class ReadingsData constructor(
    val id: Int? = null,
    val temperature: String? = null,
    val humidity: String? = null,
    val moisture: String? = null,
    val heat_index: String? = null,
    val exhaust_fan: String? = null,
    val mist_pump: String? = null,
    val heater: String? = null,
    val log_date: String? = null,
    val log_time: String? = null,
    val user_agent: String? = null
)