package com.example.vetistech.frameworks.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class APIErrorModel constructor(
    val status: Int,
    val message: String
)