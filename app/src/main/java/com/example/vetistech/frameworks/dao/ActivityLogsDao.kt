package com.example.vetistech.frameworks.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.vetistech.frameworks.entity.ActivityLogsEntity

@Dao
interface ActivityLogsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(activityLogsEntity: List<ActivityLogsEntity>)

    @Query("DELETE FROM ACTIVITY_LOGS_ENTITY")
    fun deleteAllLogs()

    @Query("SELECT * FROM ACTIVITY_LOGS_ENTITY")
    fun getAllLogs(): LiveData<List<ActivityLogsEntity>>

}