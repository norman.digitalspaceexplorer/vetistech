package com.example.vetistech.frameworks.network

import com.example.vetistech.frameworks.model.ControlsModel
import com.example.vetistech.interceptors.BaseInterceptor
import com.example.vetistech.interceptors.NetworkInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface ControlsAPI {
    @GET("api/controls")
    fun getControlsAsync(
        @Query("platform_id") platform_id: Int
    ) : Deferred<ControlsModel>
    @FormUrlEncoded
    @POST("api/controls")
    fun postControlsAsync(
        @Query("platform_id") platform_id: Int,
        @Field("mist_pump") value: String
    )  : Deferred<ControlsModel>
}

object NetworkControls {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BaseInterceptor.BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshiBuilder))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(NetworkInterceptor(OkHttpClient.Builder()).getInterceptor())
        .build()
    val service: ControlsAPI = retrofit.create(ControlsAPI::class.java)
}

