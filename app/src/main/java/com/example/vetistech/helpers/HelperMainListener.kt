package com.example.vetistech.helpers

import android.content.Intent
import android.view.View
import com.example.vetistech.*
import com.google.android.material.card.MaterialCardView

class HelperMainListener constructor(private val activity: MainActivity) : View.OnClickListener {
    override fun onClick(v: View?) {
        when (v) {
            is MaterialCardView -> {
                when (v.getTag().toString()) {
                    activity.getString(R.string.watering_can) -> {
                        val controlsDialog = WaterPumpDialog()
                        controlsDialog.show(activity.supportFragmentManager, "Water Pump")
                    }
                    activity.getString(R.string.activity_log) -> {
                        activity.startActivity(Intent(activity, ActivityLogs::class.java))
                    }
                    activity.getString(R.string.cctv) -> {
                        val ipAddress = IPAddressDialog()
                        ipAddress.show(activity.supportFragmentManager, "IP Address")
                    }
                    activity.getString(R.string.sensor_readings) -> {
                        val readingsDialog = ReadingsDialog()
                        readingsDialog.show(activity.supportFragmentManager, "Sensor Readings")
                    }
                }
            }
        }
    }
}