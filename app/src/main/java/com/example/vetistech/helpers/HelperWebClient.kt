package com.example.vetistech.helpers

import android.view.View
import android.webkit.*
import android.widget.ProgressBar
import android.widget.Toast
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import timber.log.Timber
import java.lang.Exception

class HelperWebClient constructor(private val progress: ProgressBar, private val ipAddress: String)
    : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        view?.loadUrl(request?.url.toString().trim())
        Timber.e("ShouldOverride-Request: $request")
        return super.shouldOverrideUrlLoading(view, request)
    }

    override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
        Timber.e("Request-Headers: ${request?.requestHeaders}")
        Timber.e("Request-URLs: ${request?.url}")
        try {
            var resp: WebResourceResponse? = null
            val url: String = request?.url.toString().trim()
            if (url == "http://$ipAddress/") {
                val httpClient = OkHttpClient()
                val requestBuilder: Request = Request.Builder()
                    .url(request?.url.toString().trim())
                    .addHeader("Connection", "close")
                    .addHeader(
                        "User-Agent",
                        "Mozilla/5.0 (Linux; Android 10; SM-A507FN Build/QP1A.190711.020; wv)"
                    )
                    .addHeader("Accept", "*/*")
                    .build()
                val response: Response = httpClient.newCall(requestBuilder).execute()
                resp = WebResourceResponse(
                    null,
                    response.header("content-encoding", "utf-8"),
                    response.body?.byteStream()
                )
            }
            return resp
        } catch (e: Exception) {
            Timber.e(e)
            e.printStackTrace()
        }
        return super.shouldInterceptRequest(view, request)
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        view?.let {
            Toast.makeText(it.context, "WebCam Loaded", Toast.LENGTH_SHORT).show()
            progress.visibility = View.INVISIBLE
            view.visibility = View.VISIBLE
        }
    }

    override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
        super.onReceivedError(view, request, error)
        view?.let {
            Toast.makeText(it.context, "Failed To Load WebCam", Toast.LENGTH_SHORT).show()
            progress.visibility = View.INVISIBLE
            view.visibility = View.VISIBLE
        }
    }

    override fun onReceivedHttpError(view: WebView?, request: WebResourceRequest?, errorResponse: WebResourceResponse?) {
        super.onReceivedHttpError(view, request, errorResponse)
        view?.let {
            Timber.e("Response-Status-Code: ${errorResponse?.statusCode}")
            progress.visibility = View.INVISIBLE
            view.visibility = View.VISIBLE
        }
    }

}