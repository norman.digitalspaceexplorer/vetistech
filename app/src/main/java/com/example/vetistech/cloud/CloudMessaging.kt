package com.example.vetistech.cloud

import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class CloudMessaging : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Timber.e("CloudMessaging-Token: $token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Timber.e("Message-NotificationBody: ${message.notification?.body}")
        Timber.e("Message-NotificationTitle: ${message.notification?.title}")
        filterMessageReceived(message = message)
    }

    private fun filterMessageReceived(message: RemoteMessage) {
        val intent: Intent = Intent().apply {
            action = INTENT_ACTION_SENT_MESSAGE
            putExtra(NOTIFICATION_TITLE_EXTRA, message.notification?.title)
            putExtra(NOTIFICATION_MESSAGE_EXTRA, message.notification?.body)
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    companion object {
        const val INTENT_ACTION_SENT_MESSAGE = "INTENT_ACTION_SENT_MESSAGE"
        const val NOTIFICATION_TITLE_EXTRA = "NOTIFICATION_TITLE_EXTRA"
        const val NOTIFICATION_MESSAGE_EXTRA = "NOTIFICATION_MESSAGE_EXTRA"
    }

}