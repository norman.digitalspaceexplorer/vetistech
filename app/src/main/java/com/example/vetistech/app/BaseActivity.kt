package com.example.vetistech.app

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import com.example.vetistech.MainActivity
import com.example.vetistech.R
import com.example.vetistech.cloud.CloudMessaging
import timber.log.Timber


open class BaseActivity constructor(private val classInherited: String) : AppCompatActivity() {

    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationChannel: NotificationChannel
    private lateinit var notificationBuilder: Notification.Builder
    private lateinit var pendingIntent: PendingIntent
    lateinit var broadcastReceiver: BroadcastReceiver

    companion object {
        const val CHANNEL_ID: String = "Alert_Notification"
        const val CHANNEL_DESC: String = "Notification for Arduino Alerts"
        const val NOTIFICATION_ID: Int = 8188
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("BaseActivity Created ...")
        Timber.v(classInherited)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val flaggedIntent = Intent(this, MainActivity::class.java)
            .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK }
        pendingIntent = PendingIntent.getActivity(this, 0, flaggedIntent, PendingIntent.FLAG_CANCEL_CURRENT)

        broadcastReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.let {
                    val message: String? = it.getStringExtra(CloudMessaging.NOTIFICATION_MESSAGE_EXTRA)
                    val title: String? = it.getStringExtra(CloudMessaging.NOTIFICATION_TITLE_EXTRA)
                    Timber.e("Message: $message, Title: $title")
                    showLocalNotification(title = title!!, message = message!!)
                }
            }
        }

    }

    open fun <T> onNavigateAndFinish(from: Activity, to: Class<T>) {
        startActivity(Intent(from, to))
        overridePendingTransition(R.anim.left_to_right_intent, R.anim.right_to_left_intent)
        finish()
    }

    open fun showLocalNotification(title: String, message: String) {
        notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_DESC, NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = Color.GRAY
        notificationChannel.enableLights(true)
        notificationChannel.importance = NotificationManager.IMPORTANCE_HIGH
        notificationManager.createNotificationChannel(notificationChannel)
        notificationBuilder = Notification.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.clipart1474483)
            .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.clipart1474483))
            .setContentTitle(title)
            .setContentText(message)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
        with(NotificationManagerCompat.from(this)) {
            notify(NOTIFICATION_ID, notificationBuilder.build())
        }
    }

    fun isNetworkAvailable() : Boolean {
        val connectivityManager = (getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager)
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}