package com.example.vetistech

import android.app.Activity
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.vetistech.app.BaseActivity
import com.example.vetistech.cloud.CloudMessaging
import com.example.vetistech.databinding.ActivitySplashBinding
import timber.log.Timber

class SplashActivity : BaseActivity(classInherited = "SplashActivity") {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        val animationFadeInLogo: Animation = AnimationUtils.loadAnimation(applicationContext, R.anim.fadein)
        binding.imageViewLogo.animation = animationFadeInLogo

        Handler(Looper.getMainLooper()).postDelayed({
            onNavigateAndFinish(this, MainActivity::class.java)
        }, 2000)

    }

    override fun overridePendingTransition(enterAnim: Int, exitAnim: Int) {
        super.overridePendingTransition(enterAnim, exitAnim)
        Timber.v("Pending Transition Override ...")
    }

    override fun <T> onNavigateAndFinish(from: Activity, to: Class<T>) {
        super.onNavigateAndFinish(from, to)
        Timber.v("Navigating ...")
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(CloudMessaging.INTENT_ACTION_SENT_MESSAGE)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

}