package com.example.vetistech

import android.app.Dialog
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.vetistech.app.BaseActivity
import com.example.vetistech.cloud.CloudMessaging
import com.example.vetistech.databinding.ActivityMainBinding
import com.example.vetistech.helpers.HelperMainListener
import com.example.vetistech.viewmodel.FCMViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.card.MaterialCardView
import com.google.firebase.messaging.FirebaseMessaging
import timber.log.Timber

class MainActivity : BaseActivity(classInherited = "MainActivity"), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var fcmViewModel: FCMViewModel

    companion object {
        const val scaleFromX: Float = 0.0f
        const val scaleFromY: Float = 0.0f
        const val scaleToX: Float = 1.0f
        const val scaleToY: Float = 1.0f
        const val pivotXValue: Float = 0.5f
        const val pivotYValue: Float = 0.5f
        const val animationDuration: Long = 900
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.helper = HelperMainListener(activity = this)
        fcmViewModel = ViewModelProvider(this, FCMViewModel.Factory(application))
            .get(FCMViewModel::class.java)

        binding.cardViewWater.addAnimationScale(duration = animationDuration)
        binding.cardViewActivityLog.addAnimationScale(duration = animationDuration)
        binding.cardViewCamera.addAnimationScale(duration = animationDuration)
        binding.cardViewReadings.addAnimationScale(duration = animationDuration)
        binding.buttonAbout.setOnClickListener(this)

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (task.isSuccessful) {
                    val token = task.result
                    setFCMTokenObservable(token = token)
                    return@OnCompleteListener
                }
            }).addOnCanceledListener { Timber.e("Token Cancelled") }

        setObservableModels()
    }

    private fun setFCMTokenObservable(token: String?) {
        fcmViewModel.fcmData.observe(this, {
            it?.let {
                Timber.e("FCMData-Observable-Token: ${it.token}")
                Timber.e("FCMData-Observable-TokenID: ${it.id}")
                binding.progressCircular.visibility = View.INVISIBLE
                if (it.token != token!!) {
                    fcmViewModel.deleteFCMToken()
                    fcmViewModel.putFCMToken(token = token)
                    return@observe
                }
                return@observe
            }
            fcmViewModel.putFCMToken(token = token!!)
        })
    }

    private fun setObservableModels() {
        fcmViewModel.hasAPIError.observe(this, {
            it?.let {
                if (it.hasAPIError) {
                    binding.progressCircular.visibility = View.INVISIBLE
                    Toast.makeText(applicationContext, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(CloudMessaging.INTENT_ACTION_SENT_MESSAGE)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    private fun MaterialCardView.addAnimationScale(duration: Long) {
        val scaleAnimation = ScaleAnimation(
            scaleFromX, scaleToX,
            scaleFromY, scaleToY,
            Animation.RELATIVE_TO_SELF, pivotXValue,
            Animation.RELATIVE_TO_SELF, pivotYValue
        )
        scaleAnimation.duration = duration
        startAnimation(scaleAnimation)
    }

    override fun onClick(v: View?) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_about)
        dialog.show()
    }

}