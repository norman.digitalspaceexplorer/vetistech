package com.example.vetistech.wrappers

data class APIErrorWrapper constructor(
    val hasAPIError: Boolean,
    val errorMessage: String
)