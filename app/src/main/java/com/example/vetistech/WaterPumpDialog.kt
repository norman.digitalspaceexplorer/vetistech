package com.example.vetistech

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.vetistech.app.BaseFragment
import com.example.vetistech.databinding.BottomsheetDialogWaterBinding
import com.example.vetistech.viewmodel.ControlsViewModel

class WaterPumpDialog : BaseFragment() {

    lateinit var binding: BottomsheetDialogWaterBinding
    lateinit var viewModel: ControlsViewModel

    private var mistPumpStatus: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottomsheet_dialog_water, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewsEnabled(container = 0.5f, toggle = false, visibility = View.VISIBLE)
        if (isNetworkAvailable()) {
            viewModel = ViewModelProvider(this, ControlsViewModel.Factory(application = activity!!.application))
                .get(ControlsViewModel::class.java)
            viewModel.getControls(platform_id = 1)
            binding.imageViewToggle.setOnClickListener {
                val value: String = if (mistPumpStatus == 1) { "OFF" } else { "ON" }
                setViewsEnabled(container = 0.5f, toggle = false, visibility = View.VISIBLE)
                viewModel.postControls(platform_id = 1, value = value)
            }
            setObservableModels()
            return
        }
        Toast.makeText(context, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
    }

    private fun setViewsEnabled(container: Float, toggle: Boolean, visibility: Int) {
        binding.relativeLayoutContainer.alpha = container
        binding.imageViewToggle.isEnabled = toggle
        binding.progressCircular.visibility = visibility
    }

    private fun setObservableModels() {
        viewModel.hasAPIError.observe(viewLifecycleOwner, {
            it?.let {
                if (it.hasAPIError) {
                    Toast.makeText(context, it.errorMessage, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.controlsData.observe(viewLifecycleOwner, {
            it?.let {
                setViewsEnabled(container = 1f, toggle = true, visibility = View.GONE)
                mistPumpStatus = if (it.data.mist_pump == "ON") { 1 } else { 0 }
                binding.imageViewToggle.setImageResource(
                    if (it.data.mist_pump == "ON") { R.drawable.toggle_on } else { R.drawable.toggle_off })
                Toast.makeText(context, "Mist Pump is ${it.data.mist_pump}", Toast.LENGTH_SHORT).show()
            }
        })
    }

}