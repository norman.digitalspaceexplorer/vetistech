package com.example.vetistech

import android.annotation.SuppressLint
import android.content.IntentFilter
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.vetistech.app.BaseActivity
import com.example.vetistech.cloud.CloudMessaging
import com.example.vetistech.databinding.ActivityCctvWebviewBinding
import com.example.vetistech.helpers.HelperWebClient

class CCTVActivity : BaseActivity(classInherited = "CCTVActivity") {

    lateinit var binding: ActivityCctvWebviewBinding

    private var ipAddress: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.esp32_webcam)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cctv_webview)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        if (intent.hasExtra("IP_ADDRESS")) ipAddress = intent.getStringExtra("IP_ADDRESS").toString()

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onStart() {
        super.onStart()
        binding.webViewESP32.settings.javaScriptEnabled = true
        binding.webViewESP32.webViewClient = HelperWebClient(progress = binding.progressCircular,
            ipAddress = ipAddress)
        binding.webViewESP32.loadUrl("http://$ipAddress")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(CloudMessaging.INTENT_ACTION_SENT_MESSAGE)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

}