package com.example.vetistech.interceptors

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber

abstract class BaseInterceptor : OkHttpClient() {

    companion object {
        const val USER_AGENT = "Arduino Microcontroller ADM Mobile 1.1.0 V1"
        const val BASE_URL = "https://vetistechapi.herokuapp.com/"
    }

    private val httpLoggingInterceptor = HttpLoggingInterceptor { message -> Timber.d(message) }

    fun getLoggingInterceptor() : HttpLoggingInterceptor {
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

}