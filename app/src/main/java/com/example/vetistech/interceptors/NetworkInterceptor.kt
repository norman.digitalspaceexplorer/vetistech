package com.example.vetistech.interceptors

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

class NetworkInterceptor constructor(private val httpClient: Builder)
    : BaseInterceptor(), Interceptor {

    fun getInterceptor(): OkHttpClient {
        httpClient.addInterceptor(this)
            .addInterceptor(getLoggingInterceptor())
            .connectTimeout(10L, TimeUnit.SECONDS)
            .writeTimeout(10L, TimeUnit.SECONDS)
            .readTimeout(10L, TimeUnit.SECONDS)
        return httpClient.build()
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request().newBuilder()
            .addHeader("user_agent", USER_AGENT)
            .build()
        return chain.proceed(request)
    }
}